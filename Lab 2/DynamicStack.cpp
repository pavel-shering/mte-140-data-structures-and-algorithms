#include "DynamicStack.hpp"
#include <iostream>
#include <stdio.h>
#include <stdlib.h>

typedef DynamicStack::StackItem StackItem;
const StackItem DynamicStack::EMPTY_STACK = -999;

DynamicStack::DynamicStack()
{
	items_ = new StackItem[16];
    //items_ = (StackItem*)malloc(16*sizeof(StackItem));
	capacity_ = 16;
	size_ = 0;
	init_capacity_ = 16;
}

DynamicStack::DynamicStack(unsigned int capacity)
{
	items_ = new StackItem[capacity];
    //items_ = (StackItem*)malloc(capacity*sizeof(StackItem));
	capacity_ = capacity;
	size_ = 0;
	init_capacity_ = capacity; 
}

DynamicStack::~DynamicStack()
{
	delete [] items_;
	items_ = NULL;
}

bool DynamicStack::empty() const
{
	return size_ == 0;
}

int DynamicStack::size() const
{
	return size_;
}

void DynamicStack::push(StackItem value)
{
    if (size_ ==  capacity_)
	{
		capacity_ *= 2;
        //items_ = (StackItem*)realloc(items_, capacity_*sizeof(StackItem));
        StackItem* old = items_;
		items_ = new StackItem[capacity_];
        
		for (int i = 0; i < size_; i++)
			items_[i] = old[i];
		
		items_[size_] = value;
		size_++;
		delete []old;
	}
    
    items_[size_++] = value;
}

StackItem DynamicStack::pop()
{
	if (empty())
		return EMPTY_STACK;

	if (--size_ <= capacity_/4 && capacity_/2 > init_capacity_)
	{
		StackItem* old = items_;
		capacity_ /= 2;
        //items_ = (StackItem*)realloc(items_, capacity_*sizeof(StackItem));
		items_ = new StackItem[capacity_];
		for (int i = 0; i < size_ ; i++)
				items_[i] = old[i];
        
		delete []old;
    }

    return items_[size_];
}

StackItem DynamicStack::peek() const
{
	if (empty())
		return EMPTY_STACK;
	
	return items_[size_ - 1];
}

void DynamicStack::print() const
{
	for(int i = size_-1; i >= 0; i--)
		std::cout << items_[i] << " -> ";
	std::cout << "NULL\n"; 
}
