#include "CircularQueue.hpp"
#include <iostream>
#include <stdio.h>
#include <stdlib.h>

typedef CircularQueue::QueueItem QueueItem;
const QueueItem CircularQueue::EMPTY_QUEUE = -999;

CircularQueue::CircularQueue()
{
	items_ = new QueueItem[16];
	capacity_ = 16;
	size_ = 0;
	head_ = 0;
	tail_ = 0; 
}

CircularQueue::~CircularQueue()
{
	delete [] items_;
	items_ = NULL;
}

bool CircularQueue::empty() const
{   
	return size_ == 0;
}

bool CircularQueue::full() const
{ 
	return size_ == capacity_;
}

int CircularQueue::size() const
{ 
	return size_;
}

bool CircularQueue::enqueue(QueueItem value)
{
	if (full())
		return false;
    
	items_[tail_] = value;
	tail_ = (tail_ + 1) % capacity_;
	size_++;
	return true;
}

QueueItem CircularQueue::dequeue()
{
	if (empty())
		return EMPTY_QUEUE;

	QueueItem value = items_[head_];
	head_ = (head_ + 1) % capacity_;
	size_--;
	return value;
}

QueueItem CircularQueue::peek() const
{
	if (empty())
		return EMPTY_QUEUE;
    
	return items_[head_];
}

void CircularQueue::print() const
{

	for(int i = head_; i != tail_; i = (i + 1) % capacity_)
        std:: cout << items_[i] << " -> ";
	std::cout << "NULL\n";
}
