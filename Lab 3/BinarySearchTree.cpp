#include "BinarySearchTree.hpp"
#include <iostream>

using namespace std;

typedef BinarySearchTree::DataType DataType;

BinarySearchTree::Node::Node(DataType newval)
{
    left = NULL;
    right = NULL;
    val = newval;
}

BinarySearchTree::BinarySearchTree()
{
    root_ = NULL;
    size_ = 0;
}

BinarySearchTree::~BinarySearchTree()
{
    while (root_ != NULL)
        remove(root_ -> val);
}

bool BinarySearchTree::insert(DataType val)
{
    if (exists(val))
        return false;

    if (root_ == NULL)
    {
        root_ = new Node(val);
        size_++;
        return true;
    }

    Node* currentNode = root_;
    while(currentNode -> val != val)
    {
        if(currentNode -> val > val)
        {
            if( currentNode -> left == NULL)
            {
                currentNode -> left = new Node(val);
                size_++;
                return true;
            }
            currentNode = currentNode -> left;

        }
        else
        {
            if(currentNode -> right == NULL)
            {
                currentNode -> right = new Node(val);
                size_++;
                return true;
            }
            currentNode = currentNode -> right;
        }

    }
    return false;
}

bool BinarySearchTree::remove(DataType val)
{
    if (!exists(val))
        return false;

    else
    {
        Node* currentNode = root_;
        Node* parentNode = NULL;

        if (size_ == 1)
        {
            delete currentNode;
            root_ = NULL;
            size_--;
            return true;
        }
        else if(currentNode -> val == val && (currentNode -> right == NULL || currentNode -> left == NULL))
        {
            if (currentNode -> right==NULL)
            {
                root_ = currentNode -> left;
                delete currentNode;
            }
            else
            {
                root_ = currentNode -> right;
                delete currentNode;
            }
            size_--;
            return true;
        }

        while(currentNode -> val != val)
        {
            if(val > currentNode -> val)
            {
                parentNode = currentNode;
                currentNode = currentNode -> right;
            }
            else
            {
                parentNode = currentNode;
                currentNode = currentNode -> left;
            }
        }

        if(currentNode -> right == NULL && currentNode -> left == NULL)
        {
            if(parentNode -> right == currentNode)
            {
                parentNode -> right = NULL;
                delete currentNode;
            }
            else
            {
                parentNode -> left = NULL;
                delete currentNode;
            }
        }
        else if(currentNode -> right != NULL && currentNode->left != NULL)
        {
            Node* maxNode = currentNode -> left;
            Node* parmaxNode = currentNode;

            while (maxNode -> right != NULL)
            {
                parmaxNode = maxNode;
                maxNode = maxNode -> right;
            }
            currentNode -> val = maxNode -> val;
            if (currentNode == parmaxNode)
            {
                if(maxNode -> left != NULL)
                {
                    parmaxNode -> left = maxNode -> left;
                }
                else
                {
                    parmaxNode -> left = NULL;
                }
            }
            else
            {
                if(maxNode -> left != NULL)
                {
                    parmaxNode -> right = maxNode -> left;
                }
                else
                {
                    parmaxNode -> right = NULL;
                }
            }

            delete maxNode;
            print();
        }
        else
        {
            if(currentNode -> right == NULL)
            {
                if(parentNode -> right == currentNode)
                {
                    parentNode -> right = currentNode -> left;
                    delete currentNode;
                }
                else
                {
                    parentNode -> left = currentNode -> left;
                    delete currentNode;
                }
            }

            if(currentNode -> left == NULL)
            {
                if(parentNode -> right == currentNode)
                {
                    parentNode -> right = currentNode -> right;
                    delete currentNode;
                }
                else
                {
                    parentNode -> left = currentNode -> right;
                    delete currentNode;
                }
            }
        }
        size_--;
        return true;
    }
}

bool BinarySearchTree::exists(DataType val) const
{
    if(root_ == NULL)
        return false;

    Node* visitor = root_;
    while (visitor != NULL)
    {
        if (visitor->val == val)
            return true;
        else if (val < visitor->val)
            visitor = visitor->left;
        else
            visitor = visitor->right;
    }
    return false;
}

DataType BinarySearchTree::min() const
{
    Node* visitor = root_;
    while (visitor -> left != NULL)
    {
        visitor = visitor -> left;
    }
    return visitor -> val;
}

DataType BinarySearchTree::max() const
{
    Node* visitor = root_;
    while (visitor -> right != NULL)
    {
        visitor = visitor -> right;
    }
    return visitor -> val;
}

unsigned int BinarySearchTree::size() const
{
    return size_;
}

unsigned int BinarySearchTree::depth() const
{
    return getNodeDepth(root_) - 1;
}

void BinarySearchTree::print() const
{
    printNode(root_, 0, true);
}

void BinarySearchTree::printNode(Node* n,int d,bool r) const
{
    if (n==NULL)
        return;

    printNode (n->right,d+1,true);
    for (int i = 0;i < d; i++)
        cout<< "  ";
    if (d!=0)
       {
           if(!r)
                cout << "\\";
           else
               cout << "/";
       }
       cout << n->val << endl;
       printNode (n->left,d+1,false);
}

int BinarySearchTree::getNodeDepth(Node* n) const
{
    if(n == NULL )
        return 0;

    int maxRight = getNodeDepth(n -> right);
    int maxLeft = getNodeDepth(n -> left);

    if(maxRight > maxLeft)
        return maxRight + 1;
    else
        return maxLeft + 1;
}

