#include "SequentialList.hpp"
#include <iostream>
#include <stdio.h>
#include <stdlib.h>


SequentialList::SequentialList(unsigned int cap)
{
	data_ = new DataType[cap];
	capacity_ = cap;	
	size_ = 0;
}

SequentialList::~SequentialList()
{
	delete [] data_;
	data_ = NULL;
}

bool SequentialList::empty() const
{
	return size_ == 0;
}

unsigned int SequentialList::size() const
{
	return size_;
}

unsigned int SequentialList::capacity() const
{
	return capacity_;
}

bool SequentialList::full() const
{
	return size_ == capacity_;
}

void SequentialList::print() const
{
	for(int i = 0; i < size_; i++)
		std:: cout << data_[i] << std::endl;
    std::cout << "NULL\n";

}

bool SequentialList::insert_front(DataType val)
{
	if (full())
        return false;
    else
    {
        for (int i = size_; i > 0;  i--)
            data_[i] = data_[i - 1];
        
        data_[0] = val;
        size_++;
        return true;
    }
}

bool SequentialList::remove_front()
{
	if (empty())
        return false;
    else
    {
        for (int i = 1; i > size_; i++)
            data_[i - 1] = data_[i];
        
        size_--;
        return true;
    }
}

bool SequentialList::insert_back(DataType val)
{
	if (full())
        return false;
    else
    {
        data_[size_] = val;
        size_++;
        return true;
    }
}

bool SequentialList::remove_back()
{
	if (empty())
        return false;
    else
    {
        size_--;
        return true;
    }
}

bool SequentialList::insert(DataType val, unsigned int pos)
{
    if (full() || pos > size_)
        return false;
	else
	{
		for( int i = size_; i > pos; i--)
			data_[i] = data_[i - 1];
		
		data_[pos] = val;
		size_++;
		return true;
	}
}

bool SequentialList::remove(unsigned int pos)
{
    
    if (empty() || pos > size_ - 1)
        return false;
    else
	{
		for(int i = pos + 1; i < size_; i++)
			data_[i - 1] = data_[i];

		size_--;
		return true;
	}
}

unsigned int SequentialList::search(DataType val) const
{
	for (int i = 0; i < size_; i++)
        if (data_[i] == val) return i;
    
    return size_;
}

SequentialList::DataType SequentialList::select(unsigned int pos) const
{
	if(pos > size_ - 1)
		return data_[size_ - 1];
	else
        return data_[pos];
}

bool SequentialList::replace(unsigned int pos, DataType val)
{
	if (pos > size_ - 1)
        return false;
    else
    {
        data_[pos] = val;
        return true;
    }
}
